r = (l...) -> global[lib.replace /-,*/,''] = require lib for lib in l
r 'jsdom','async','fs','jade'
R = require 'ramda'
history = []
email = if process.env.EMAIL then process.env.EMAIL else 'slava.ganzin@gmail.com'
Mailgun = require('mailgun').Mailgun
mg = new Mailgun('key-816415c119e7584ba45daa2e77e6d848')
exec = require('child_process').exec
dbFile = process.env.HOME + '/collector-links.json'

e = (f) -> (e, r) ->
  console.log e if e
  throw e if e
  f r

D = (a) -> console.log a; a
letter = jade.compileFile './letter.jade'

finish = (e, r) ->
  console.log r
  r = R.mergeAll r
  send r if r and R.keys(r).length

jquery = fs.readFileSync __dirname+"/jquery.js", "utf-8"

filterText = (e) ->
  e = e.toLowerCase()
  (/vernis|ногт|\sлак\s|polish/.test(e) ||
  (/румяна|joues contraste/.test(e) &&
    not /cream|crem|кремовые|шарики|в шариках|шариковые|в наборе|набор/.test e))

send = (links) ->
  content = letter links: links

  mg.sendRaw '<postmaster@sandbox84c9cce2facd45f6b08ab8ed717d134f.mailgun.org>',
        [email],
        'From: slava.ganzin@gmail.com' +
        '\nTo: ' + email +
        '\nContent-Type: text/html; charset=utf-8' +
        '\nSubject: Новые лаки\n\n' +
        content,
        (e) -> e && console.log e

parse = (selector, linkSelector, imageSelector)-> (c)-> (w)->
  cutHash = (url) -> url.replace /#[^#]+$/, ''
  prependShafa = (uri) ->
    if /^\//.test uri
      "http://shafa.ua#{uri}"
    else
      uri

  return c [] if !! w.$('body:contains("Не найдено")').length
  links = {}
  w.$(selector).each ->
    url = w.$(this).find(linkSelector).attr('href')
    return unless url
    k = prependShafa cutHash url
    text =  w.$(this).find(linkSelector).text()
      .replace(/\s+/g, ' ').replace(/(^ | $)/g, '')
    image = prependShafa w.$(this).find(imageSelector).attr('src')
    links[k] = text: text, image: image if filterText text
  c links

polishSale = nailSale = (f) -> (c) -> (w) ->
  # TODO: 2014
  links = {}
  w.$('item').each ->
    if (f w.$(this))
      k = w.$(this).find('guid').text()
      links[k] = text: w.$(this).find('title').text()
  c links

olx      = parse '.offer', '.detailsLink', '.fleft'
kidstaff = parse '.hr1  tr', '.descript:not(.mya)', '.cleft img'
# klubok = parse '.product-widget > a'
shafa    = parse '.grid3', '.item-container', '.item-thumbnail'


search = ([url, query, f], done) ->
  if /kidstaff/.test url
    return done null, []
    # dom = (c) ->
    #   exec 'curl -s "#{url}#{query}" | iconv -f WINDOWS-1251', e (r) ->
    #     jsdom.env html: r, src: [jquery, "$('a').each(function(){
    #       $(this).attr('href',
    #        'http://www.kidstaff.com.ua' + $(this).attr('href'));
    #     });
    #     $('tr').filter(function(){
    #       return $(this).attr('bgcolor') == '#fffaf0';
    #     }).each(function(){
    #       $(this).html('');
    #     });"], done: c
    #
  else
    dom = (c) -> jsdom.env url: url + encodeURI(query), src: [jquery], done: c

    dom e f (links) ->
      links   = R.pick R.difference(R.keys(links), R.keys(history)), links
      history = R.merge history, links
      fs.writeFile dbFile, JSON.stringify history
      done null, links

try
  history = JSON.parse fs.readFileSync dbFile
catch
  fs.writeFileSync dbFile, history = {}

async.map [

          ['http://shafa.ua/women?view=list&search_text=', 'chanel', shafa]
          ['http://shafa.ua/women?page=2&view=list&search_text=', 'chanel', shafa]
          ['http://shafa.ua/women?page=3&view=list&search_text=', 'chanel', shafa]
          ['http://shafa.ua/women?page=4&view=list&search_text=', 'chanel', shafa]
          ['http://shafa.ua/women?page=5&view=list&search_text=', 'chanel', shafa]
          ['http://www.kidstaff.com.ua/search.php?mode=search&words=',
           'лак+шанель', kidstaff]
          ['http://www.kidstaff.com.ua/search.php?mode=search&words=',
           'лак+chanel', kidstaff]
          ['http://www.kidstaff.com.ua/search.php?mode=search&words=',
           'chanel+le+vernis',  kidstaff]

          ['http://olx.ua/list/q-', 'chanel', olx]
          ['http://olx.ua/list/q-', 'chanel le vernis', olx]
          ['http://olx.ua/list/q-', 'лак шанель', olx]
          # ['http://klubok.com/?q=', 'лак сhanel', klubok]
          # ['http://klubok.com/?q=', 'лак chanel', klubok]
          # ['http://klubok.com/?q=', 'chanel le vernis', klubok]
          ['http://polish-sale-ua.livejournal.com/data/rss?tag=',
            'марка Chanel', polishSale -> true]
          ['http://nail-sale.livejournal.com/data/rss?tag=',
            'города Украины', nailSale ($this) ->
              !! $this.find("category:contains('марка CHANEL')").length ]

          ], search, finish
